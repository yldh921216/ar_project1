﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playTypeManager : MonoBehaviour
{
    public enum PlayType { marker, noMarker }

    public PlayType playType;


    private void Start()
    {
        var playTypeManagerScripts = FindObjectsOfType<playTypeManager>();

        if(playTypeManagerScripts.Length ==1 )
        {
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }



}
