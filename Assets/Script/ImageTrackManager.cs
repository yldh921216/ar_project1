﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class ImageTrackManager : MonoBehaviour
{
   public enum Phase { phase1, phase2 }

    [Header("Camera")]
    public GameObject arSession;
    public GameObject arSession2;
    public GameObject noMarkerCamera;
    public GameObject noMarkerSpawnButton;


    public ARTrackedImageManager arTrackManager;
    bool tracked = false;
    public GameObject emptyBallon;
    public GameObject trackGuidPanel;
    public AudioSource audioSource;
    public AudioClip checkTheMarkerAudio;
    public AudioClip spawnBallonAudio;
    public bool Spawned;

    public GameObject fireButton;
    public AudioClip clickTheFireAudio;
    public bool fired;

    public AudioClip congratulationAudio;
    public bool fulled;
    public GameObject throwBallonButton;

    public AudioClip throwTheBallonAudio;
    public bool ballonSceneDone;
    public GameObject endingBallons;

    public AudioClip goToLapAudio;
    public GameObject goToLabButton;
    public GameObject labPrefab;
    public GameObject lyricsGameObject;
    Text lyricsText;

    public ParticleSystem explosionParticle;
    public ParticleSystem smokeParticle;

    [Header("Plask Lab")]

    public Transform spawnPosition;


    public AudioClip alcholAudio;
    public AudioClip alcholSelectedAudio;
    public GameObject alcholLampButton;
    public GameObject alcholLampObj;
    bool lampClicked;

    public AudioClip threeLegAudio;
    public AudioClip threeLegSelectedAudio;
    public GameObject threeLegButton;
    public GameObject threeLegObj;
    bool threeLegClicked;

    public AudioClip plaskAudio;
    public AudioClip plaskSelectedAudio;
    public GameObject plaskButton;
    public GameObject plaskObj;
    bool plaskClicked;


    public Animator ballonBlowUpAnimator;
    public AudioClip ballonToPlaskAudio;
    public AudioClip afterBallonToPlaskAudio;
    public AudioClip showPlaskAir;
    public GameObject ballonToPlaskButton;
    public GameObject ballonToPlaskObj;
    public Transform iceBucketMeetPlaskPos;
    bool ballonToPlaskClicked;

    public AudioClip IgniteToAlcholAudio;
    public GameObject igniteButton;
    public GameObject lampFire;
    public Transform lampFireTransform;
    public ParticleSystem explosionRed;
    public bool igniteClicked;
    
    public AudioClip ballonBlowUpAudio;
    bool ballonBlowUpDone;

    public AudioClip turnOffFireAudio;
    public GameObject turnOffButton;
    bool turnOffFireClicked;

    public AudioClip selectIceAudio;
    public GameObject iceButton;
    public GameObject iceBasket;
    public GameObject ballonBlowUpPlask;
    public AudioClip iceSpawnEffectSound;
    bool iceButtonClicked;
    bool ballonSizeDownDone;

    public AudioClip plaskToIceAudio;
    public AudioClip plaskToIceDoneAudio;
    public AudioClip iceCrackingEffectSound;
    public GameObject plaskToIceButton;
    bool plaskToIceClicked;
    public  bool moveDone;

    public GameObject centerSpawnPoses;


    public AudioClip endingOfLabAudio;
    public AudioClip endingSoundEffect;

    [Header("RestartUI")]
    public GameObject endRestartButton;
    public GameObject restartButton;

    Phase phase;

    public GameObject[] labObjects;

    [Header ("Public Effect Sounds")]
    public AudioClip buttonClickSound;
    public AudioClip[] successEffectSounds;
    public AudioClip endEffectSound;
    

    [Header("Ballon Effect sounds")]
    public AudioClip windSoundEffect;
    public AudioClip igniteSoundEffect;
    public AudioClip ballonBlowUpSoundEffect;

    [Header("Lab Effect Sounds")]
    

    [Header("BGM")]
    public AudioClip endingBGM;

    StartManager startManagerScript;
    playTypeManager playTypeManagerScript;
    

    float m_fOldToucDis = 0f;       
    float m_fFieldOfView = 60f;     

    void Update()
    {
        while (!startManagerScript)
        {
            return;
        }

       if(playTypeManagerScript.playType == playTypeManager.PlayType.noMarker)
        {
            CheckTouch();
        }    

       if( buttonPanel.activeInHierarchy && (  wrongCount > 3 || buttonPanelScript.duration >5 ))
        {
            if (blowUpTellingPart == BlowUpTelling.alchol)
            {
               StartCoroutine(  Flash(alcholLampButton) ) ;
                wrongCount = 0;
                timeOut = 0;
            }
            else if (blowUpTellingPart == BlowUpTelling.threeLeg)
            {
                StartCoroutine(Flash(threeLegButton));
                wrongCount = 0;
                timeOut = 0;
            }
            else if (blowUpTellingPart == BlowUpTelling.plask)
            {
                StartCoroutine(Flash(plaskButton));
                wrongCount = 0;
                timeOut = 0;
            }
            else if (blowUpTellingPart == BlowUpTelling.ballon)
            {
                StartCoroutine(Flash(ballonToPlaskButton));
                wrongCount = 0;
                timeOut = 0;
            }
            else if(blowUpTellingPart == BlowUpTelling.Ignite)
            {
                StartCoroutine(Flash(igniteButton));
                wrongCount = 0;
                timeOut = 0;
            }


            if (blowDownTellingPart == BlowDownTelling.turnOff)
            {
                StartCoroutine(Flash(turnOffButton));
                wrongCount = 0;
                timeOut = 0;
            }
            else if (blowDownTellingPart == BlowDownTelling.Ice)
            {
                StartCoroutine(Flash(iceButton));
                wrongCount = 0;
                timeOut = 0;
            }
            else if (blowDownTellingPart == BlowDownTelling.plaskToIce)
            {
                StartCoroutine(Flash(plaskToIceButton));
                wrongCount = 0;
                timeOut = 0;
            }


        }


    }

    public bool correctButtonClicked;
    public IEnumerator Flash (GameObject button)
    {
        print("flash");
        Image imageColor = button.GetComponent<Image>();

        Color originColor = imageColor.color;
        Color newColor = new Color(0, 1, 1);
        
        while(!correctButtonClicked)
        {
            imageColor.color = originColor;
            yield return new WaitForSeconds(0.3f);
            imageColor.color = newColor;
        }

        imageColor.color = originColor;

    }

    public IEnumerator WrongFlash (GameObject button)
    {
        Image imageColor = button.GetComponent<Image>();

        Color originColor = imageColor.color;
        Color newColor = new Color(1,0,0);
        float timer = 0;
        while (timer > 1)
        {
            imageColor.color = originColor;
            yield return new WaitForSeconds(0.1f);
            imageColor.color = newColor;
            timer += 0.1f;
        }

        imageColor.color = originColor;

    }
    private void OnEnable()
    {
        playTypeManagerScript = FindObjectOfType<playTypeManager>();

        if (playTypeManagerScript.playType == playTypeManager.PlayType.marker)
        {
            print("marker");
            arSession.SetActive(true);
            arSession2.SetActive(true);
            noMarkerCamera.SetActive(false);
            arTrackManager.trackedImagesChanged += ImageChanged;

        }
        else
        {
            print("no marker");
            arSession.SetActive(false);
            arSession2.SetActive(false);
            noMarkerCamera.SetActive(true);
        }
    }
    void CheckTouch()
    {
        int nTouch = Input.touchCount;
        float m_fToucDis = 0f;
        float fDis = 0f;

        if (Input.touchCount == 2 && (Input.touches[0].phase == TouchPhase.Moved || Input.touches[1].phase == TouchPhase.Moved))
        {
            m_fToucDis = (Input.touches[0].position - Input.touches[1].position).sqrMagnitude;

            fDis = (m_fToucDis - m_fOldToucDis) * 0.01f;

            m_fFieldOfView -= fDis;

            m_fFieldOfView = Mathf.Clamp(m_fFieldOfView, 20.0f, 100.0f);

            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, m_fFieldOfView, Time.deltaTime * 5);

            m_fOldToucDis = m_fToucDis;
        }
    }
    public void SuccessRandomSoundEffect()
    {
        audioSource.PlayOneShot(successEffectSounds[Random.Range(0, successEffectSounds.Length - 1)]);
    } 

    public void PlayButtonClickSound()
    {
        audioSource.PlayOneShot(buttonClickSound);
    }

    
    public void EndRestartButton()
    {
        endRestartButton.SetActive(false);
        PlayButtonClickSound();
        SceneManager.LoadScene(0);     
    }

    Vector3 newRot = new Vector3(30.454f, 4.842f, 0.587f );

    public buttonPanel buttonPanelScript;
    private void Start()
    {
        buttonPanelScript = FindObjectOfType<buttonPanel>();
        
        lyricsText = lyricsGameObject.GetComponentInChildren<Text>();
        print(lyricsText);
        StartCoroutine(StoryTellerBallon());
    }


    private void OnDisable()
    {
        if (playTypeManagerScript.playType == playTypeManager.PlayType.marker)
        {
            arTrackManager.trackedImagesChanged -= ImageChanged;
        }
    }


    private void ImageChanged ( ARTrackedImagesChangedEventArgs eventArgs )
    {
       // ReOrderPositionObject();

        foreach ( ARTrackedImage trackedImage in eventArgs.updated)
        {
            UpdateImage(trackedImage);
        }
    }

    void UpdateImage(ARTrackedImage trakedImage)
    {
        spawnPosition.position = trakedImage.transform.position;
        spawnPosition.rotation = trakedImage.transform.rotation;
        print( $"spawnPos : {spawnPosition.position} ");
        print( $"plaskObj : {plaskObj.transform.position}");
        print( $"ThreeLegObj : {threeLegObj} ");

        if(phase == Phase.phase1)
        {
            emptyBallon.SetActive(true);
            emptyBallon.transform.position = trakedImage.transform.position;
        }
        else
        {
            emptyBallon.SetActive(false);
            ReOrderPositionObject();
        }
        
        trackGuidPanel.SetActive(false);
        Spawned = true;        
    }

    public void OnClickNoMarkerSpawnButton()
    {
        Spawned = true;
        emptyBallon.SetActive(true);
        noMarkerSpawnButton.SetActive(false);
        emptyBallon.transform.position = spawnPosition.position;
    }

    public void CameraSetting ()
    {
        noMarkerCamera.transform.position = new Vector3(-0.021f, 0.262f, 0.656f);
        noMarkerCamera.transform.DORotate(newRot, 1);
        noMarkerCamera.GetComponent<Camera>().fieldOfView = 37;
    }

    public void OnClickFireButton()
    {
        PlayButtonClickSound();
        fired = true;
        fireButton.SetActive(false);
        StartCoroutine(FullBallon());
    }

    public Animator airBallonAnimaotr;
    public GameObject airBallonFire;
    public AudioClip airBallonBlowUpDoneAudio;

    IEnumerator FullBallon ()
    {
        yield return null;
        fulled = true;
    }

    public void OnClickThrowBallonButton ()
    {
        PlayButtonClickSound();
        StartCoroutine(EndingSceneOfBallon());
        throwBallonButton.SetActive(false);
    }
    public GameObject[] airBallons;
    public AudioClip spawnSoundEffect;
    public AudioClip ballonBiggerSoundEffect;
    public AudioClip beautifulAirballonsAudio;
    public AudioClip funnyLabAudio;
    public Transform[] spawnPoses;
    public ParticleSystem starBurstParticle;

    public IEnumerator EndingSceneOfBallon()
    {
        emptyBallon.transform.DOMove(new Vector3(emptyBallon.transform.position.x, emptyBallon.transform.position.y + 1, emptyBallon.transform.position.z), 35);

        yield return new WaitForSeconds(1.5f);

        if(playTypeManagerScript.playType == playTypeManager.PlayType.noMarker)
        {
            Vector3 cameraOriPos = noMarkerCamera.transform.position;
            noMarkerCamera.transform.DORotate(new Vector3(-42.959f, 4.073f, 0.692f), 15);
        }
        float spawnTimer = 2;
        yield return new WaitForSeconds(2.3f);

        for ( int x = 0; x<airBallons.Length; x++)
        {
                spawnTimer -= 0.3f;
                if (spawnTimer < 0) spawnTimer = 0;
                airBallons[x].SetActive(true);
                audioSource.PlayOneShot(spawnSoundEffect);
                airBallons[x].transform.position = spawnPoses[x].transform.position;            
                starBurstParticle.gameObject.transform.position = spawnPoses[x].transform.position;
                starBurstParticle.Play();
                airBallons[x].transform.DOMove(new Vector3(airBallons[x].transform.position.x, airBallons[x].transform.position.y + 2, airBallons[x].transform.position.z), 20);
                yield return new WaitForSeconds(spawnTimer);            
        }

        audioSource.PlayOneShot(endingSoundEffect);
        yield return new WaitForSeconds(3);

        ballonSceneDone = true;

    }

    public GameObject chooseLabTypePanel;
   

    public void OnClickLabStartButton()
    {
        PlayButtonClickSound();
        //labPrefab.SetActive(true);
        goToLabButton.SetActive(false);
        StartCoroutine(DestroyAirBallons());

        chooseLabTypePanel.SetActive(true);
    }

    public void OnClickBlowUpExpButton()
    {
        StartCoroutine(BlowUpTeller());
        chooseLabTypePanel.SetActive(false);
        audioSource.PlayOneShot(buttonClickSound);
    }
    public void OnClickBlowDownExpButton()
    {
        StartCoroutine(BlowDownTeller());
        chooseLabTypePanel.SetActive(false);
        audioSource.PlayOneShot(buttonClickSound);
    }


    IEnumerator DestroyAirBallons ()
    {
        for (int x = 0; x < airBallons.Length; x++)
        {
            airBallons[x].SetActive(false);
            audioSource.PlayOneShot(spawnSoundEffect);
            airBallons[x].transform.position = spawnPoses[x].transform.position;
            starBurstParticle.gameObject.transform.position = spawnPoses[x].transform.position;
            starBurstParticle.Play();
            yield return new WaitForSeconds(0.1f);
        }
    }


    IEnumerator TextActing (string content , float duration)
    {
        lyricsText.text = "";
        float interval = 0;
        interval = duration / content.Length;

        foreach (var textChar in content)
        {
            lyricsText.text += textChar;
            //audioSource.PlayOneShot(keyboardSounds[Random.Range(0, keyboardSounds.Length-1)]);
            yield return new WaitForSeconds(interval);   
        }
    }
   


    IEnumerator StoryTellerBallon ()
    {
        phase = Phase.phase1;

        if(playTypeManagerScript.playType == playTypeManager.PlayType.noMarker)
        {
            trackGuidPanel.SetActive(false);
            noMarkerSpawnButton.SetActive(true);
            lyricsGameObject.SetActive(true);
            lyricsText.text = "버튼을 눌러주세요!!!!";
        }
        else
        {
            yield return new WaitForSeconds(1f);
            audioSource.PlayOneShot(checkTheMarkerAudio);
        }

        while(!Spawned)
        {
            yield return null;  
        }
        Spawned = false;
        
        lyricsGameObject.SetActive(true);
        audioSource.PlayOneShot(spawnBallonAudio);
        explosionParticle.gameObject.SetActive(true);
        explosionParticle.Play();
        lyricsText.text = "열기구가 나타났습니다! 그런데 열기구에 바람이 빠젔네요";

        audioSource.PlayOneShot(successEffectSounds[0]);
        //StartCoroutine(TextActing("열기구가 나타났습니다! 그런데 열기구에 바람이 빠젔네요", 3.5f));
        yield return new WaitForSeconds(4);
        
        lyricsText.text = "열기구 토치에 불을 붙혀 열기구를 팽창 시킬거에요!";
       //StartCoroutine(TextActing("열기구 토치에 불을 붙혀 열기구를 팽창 시킬거에요!", 4));

        yield return new WaitForSeconds(5);
       
        lyricsText.text = "준비 됐나요??";
        //StartCoroutine(TextActing("준비 됐나요?", 3));
        yield return new WaitForSeconds(4);

        lyricsGameObject.SetActive(false);  

        
        audioSource.PlayOneShot(clickTheFireAudio);
        lyricsGameObject.SetActive(true);
        lyricsText.text = "불을 눌러주세요!!!";
        //StartCoroutine(TextActing("불을 눌러주세요!!", 2));
        yield return new WaitForSeconds(2);
        fireButton.SetActive(true);

        lyricsText.text = "공기가 활발히 움직이면서 열기구를 팽창시킬거에요!!!";
        //StartCoroutine(TextActing("공기가 활발히 움직이면서 열기구를 팽창시킬거에요!!!",3));
        yield return new WaitForSeconds(4);
        
        lyricsGameObject.SetActive(false);

        while (!fulled)
        {
            yield return null;
        }

        
        audioSource.PlayOneShot(igniteSoundEffect);
        airBallonFire.SetActive(true);
        fulled = false;

        emptyBallon.transform.rotation = new Quaternion(0, 0, 0, 0);
        airBallonAnimaotr.SetBool("BallonUp", true);
        audioSource.PlayOneShot(successEffectSounds[0]);
        audioSource.PlayOneShot(congratulationAudio);
        lyricsGameObject.SetActive(true);
        lyricsText.text = "성공했어요! 축하합니다!!";
        //StartCoroutine(TextActing("성공했어요! 축하합니다!!", 2));
        yield return new WaitForSeconds(3.5f);
        audioSource.PlayOneShot(airBallonBlowUpDoneAudio);
        //StartCoroutine(TextActing("열기구가 부풀어 올랐습니다!!!!", 2f));
        lyricsText.text = "열기구가 부풀어 올랐습니다!!!!";
        yield return new WaitForSeconds(3.5f);
        lyricsGameObject.SetActive(false);

        audioSource.PlayOneShot(throwTheBallonAudio);
        lyricsGameObject.SetActive(true);
        lyricsText.text = "부풀어 오른 열기구를 날려 볼게요! 바람 버튼을 눌러주세요!!! ";
        //StartCoroutine(TextActing("부풀어 오른 열기구를 날려 볼게요! 바람 버튼을 눌러주세요!!!", 3));
        yield return new WaitForSeconds(4);
        throwBallonButton.SetActive(true);
        lyricsGameObject.SetActive(false);

        //SuccessRandomSoundEffect();
        //ballon Wind Animation;

        while (!ballonSceneDone)
        {
            yield return null;
        }
        ballonSceneDone = false;
       
        
        lyricsGameObject.SetActive(true);
        audioSource.PlayOneShot(beautifulAirballonsAudio);
        //StartCoroutine(TextActing("아름답네요!!! 열기구에 타보고 싶지 않나요? ", 2f));
        lyricsText.text = "아름답네요!!! 열기구에 타보고 싶지 않나요? ";
        yield return new WaitForSeconds(3f);
        //StartCoroutine(TextActing("그러면 다음으로", 1));
        lyricsText.text = "그러면 다음으로";
        yield return new WaitForSeconds(2.5f);

        audioSource.PlayOneShot(goToLapAudio);
        
        lyricsText.text = "실험실에서 열기구가 부풀어 오르는 원리를 알아볼까요? " ;
        //StartCoroutine(TextActing("실험실에서 열기구가 부풀어 오르는 원리를 알아볼까요? ", 4f));
        yield return new WaitForSeconds(4);
       
        //lyricsText.text = "실험실로 고고!!";
        StartCoroutine(TextActing("실험실로 고고!!", 2));
        yield return new WaitForSeconds(3);
        goToLabButton.SetActive(true);

        
        emptyBallon.SetActive(false);
        lyricsGameObject.SetActive(false);

    }

    public void ReOrderPositionObject()
    {
       for( int x = 0; x<labObjects.Length; x++)
        {
            if (labObjects[x].activeInHierarchy)
            {
                if(labObjects[x] == plaskObj )
                {
                    labObjects[x].transform.position = new Vector3(threeLegObj.transform.position.x, threeLegObj.transform.position.y + 0.0585f, threeLegObj.transform.position.z);
                    labObjects[x].transform.rotation = spawnPosition.rotation;
                }             
                else
                {
                    labObjects[x].transform.position = spawnPosition.position;
                    labObjects[x].transform.rotation = spawnPosition.rotation;
                }          
            }
        }
    }
    public void OnClickLampButton()
    {
        PlayButtonClickSound();
   
        if(blowUpTellingPart == BlowUpTelling.alchol)
        {
            correctButtonClicked = true;
            explosionParticle.Play();
            // alcholLampButton.SetActive(false);
            alcholLampObj.SetActive(true);
            alcholLampObj.transform.position = spawnPosition.position;
            ReOrderPositionObject();
            buttonPanel.SetActive(false);
            lampClicked = true;
        }
        else
        {
            wrongCount += 1;
            StartCoroutine( WrongFlash(alcholLampButton));
        }
    }
    public void OnClickThreeLegButton()
    {
        PlayButtonClickSound();

        if (blowUpTellingPart == BlowUpTelling.threeLeg)
        {
            explosionParticle.Play();
            //threeLegButton.SetActive(false);
            threeLegObj.SetActive(true);
            threeLegObj.transform.position = spawnPosition.position;
            ReOrderPositionObject();
            threeLegClicked = true;
            buttonPanel.SetActive(false);   
        }
        else
        {
            wrongCount += 1;
            StartCoroutine(WrongFlash(threeLegButton));
        }
    }
    public void OnClickPlaskButton()
    {
        PlayButtonClickSound();

        if (blowUpTellingPart == BlowUpTelling.plask)
        {
            buttonPanel.SetActive(false);
            explosionParticle.Play();
            plaskObj.SetActive(true);
            ReOrderPositionObject();
            //ReOrderPositionObject();
            //plaskButton.SetActive(false);
            plaskClicked = true;
        }
        else
        {
            wrongCount += 1;
            StartCoroutine(WrongFlash(plaskToIceButton));
        }

    }
    public void OnClickBallonToPlaskButton()
    {
        PlayButtonClickSound();
        if (blowUpTellingPart == BlowUpTelling.ballon)
        {
            buttonPanel.SetActive(false);
            explosionParticle.Play();
            ballonToPlaskObj.SetActive(true);
            //ballonToPlaskButton.SetActive(false);
            ballonToPlaskClicked = true;
        }
        else
        {
            wrongCount += 1;
            StartCoroutine(WrongFlash(ballonToPlaskButton));
        }
    }
    public void OnClickIgniteButton()
    {
        PlayButtonClickSound();

        if (blowUpTellingPart == BlowUpTelling.Ignite)
        {

            buttonPanel.SetActive(false);
            //igniteButton.SetActive(false);
            explosionRed.gameObject.SetActive(true);
            ReOrderPositionObject();
            explosionRed.gameObject.transform.position = lampFireTransform.position;
            explosionRed.gameObject.transform.rotation = lampFireTransform.rotation;
            explosionRed.Play();
            lampFire.SetActive(true);
            igniteClicked = true;
        }
        else
        {
            wrongCount += 1;
            StartCoroutine(WrongFlash(igniteButton));
        }
    }


    public void OnClickTurnOffButton()
    {
        PlayButtonClickSound();
        lampFire.SetActive(false);
        smokeParticle.gameObject.SetActive(true);
        //turnOffButton.SetActive(false);
        turnOffFireClicked = true;
    }
    public void OnClickIceButton()
    {
        PlayButtonClickSound();
        //iceButton.SetActive(false);
        iceButtonClicked = true;
    }
    public void OnClickPlaskToIceButton()
    {
        PlayButtonClickSound();
        plaskToIceClicked = true;
        //plaskToIceButton.SetActive(false);

    }


    public GameObject[] airsParents;
    public GameObject[] airs;
    public Transform[] plaskPoses;
    public Transform plaskExitPos;
    public Transform plaskExitPos2;
    public Transform[] BallonPoses;


    IEnumerator AirBlowUpAnimation ( )
    {
        while(!igniteClicked)
        {
            for ( int x =0; x< airs.Length; x++)
            {
                airsParents[x].transform.DOMove(plaskPoses[Random.Range(0,plaskPoses.Length-1)].position , 4f);  
            }
            yield return new WaitForSeconds(2f);
        }
        print("ignite");
        igniteClicked = false;


        //for( int x = 0; x<airs.Length; x++)
        //{
        //    airsParents[x].transform.DOMove(plaskExitPos2.position, 0.5f);
        //}
        //yield return new WaitForSeconds(0.5f);
   
        for (int x= 0; x<airs.Length; x++)
        {
            if ( x < BallonPoses.Length)
            {
                airsParents[x].transform.DOMove(BallonPoses[x].position, 5f);
                print("");
            }        
            else
            {
                airsParents[x].transform.DOMove(BallonPoses[Random.Range(0, BallonPoses.Length - 1)].position, 5f);
            }
   
            airs[x].transform.DOShakePosition(300, 0.2f, 10);
        }
    }
    public GameObject[] blowDownAirsParents;
    public GameObject[] blowDownAirs;
    public Transform[] blowDownPlaskPoses;
    public Transform blowDownPlaskExitPos;
    public Transform blowDownPlaskExitPos2;
    public Transform[] blowDownBallonPoses;
    public Animator blowDownAnimator;


    public GameObject buttonPanel;
    public int wrongCount;
    public int timeOut;
    public enum BlowUpTelling { alchol , threeLeg , Ignite , plask, ballon }
    public BlowUpTelling blowUpTellingPart;

    public enum BlowDownTelling { turnOff, Ice, plaskToIce }
    public BlowDownTelling blowDownTellingPart;

    public GameObject[] allGameObjects;
    public void LapTypeSelectPanel()
    {

        if(phase == Phase.phase1)
        {
            SceneManager.LoadScene(0);
        }

        else
        {
            chooseLabTypePanel.SetActive(true);

            for (int x = 0; x < allGameObjects.Length; x++)
            {
                allGameObjects[x].SetActive(false);
            }
        }
       
    }

    IEnumerator AirBlowDownAnimation()
    {
      for (int x = 0; x < blowDownAirs.Length; x++)
      {
         blowDownAirs[x].transform.DOShakePosition(15, 0.2f, 10);
       }

        while (!moveDone)
        {
            yield return null;
        }

        for (int x = 0; x < blowDownAirs.Length; x++)
        {
            blowDownAirs[x].transform.DOShakePosition(12, 0.02f, 10);
            blowDownAirsParents[x].transform.DOMove(blowDownPlaskExitPos2.position, 3);
        }
        yield return new WaitForSeconds(2);

        for (int x = 0; x < blowDownAirs.Length; x++)
        {
            blowDownAirsParents[x].transform.DOMove(blowDownPlaskExitPos.position, 2);
        }

        yield return new WaitForSeconds(1);

        for (int x = 0; x < airs.Length; x++)
        {
            if (x < blowDownPlaskPoses.Length)
            {
                blowDownAirsParents[x].transform.DOMove(blowDownPlaskPoses[x].position, 5f);               
            }
            else
            {
                blowDownAirsParents[x].transform.DOMove(blowDownPlaskPoses[Random.Range(0, blowDownPlaskPoses.Length - 1)].position, 5f);
            }
        }

        while (ballonBlowUpPlask.activeInHierarchy)
        {
            print("");
            for (int x = 0; x < airs.Length; x++)
            {
                blowDownAirsParents[x].transform.DOMove(blowDownPlaskPoses[Random.Range(0, blowDownPlaskPoses.Length - 1)].position, 10f);
            }
            yield return new WaitForSeconds(2.5f);
        }

    }

    IEnumerator BlowUpTeller()
    {
        phase = Phase.phase2;

        if (playTypeManagerScript.playType == playTypeManager.PlayType.noMarker)
        {
            emptyBallon.SetActive(false);
            CameraSetting();
        }

        yield return new WaitForSeconds(1);

        lyricsGameObject.SetActive(true);
        audioSource.PlayOneShot(funnyLabAudio);
        //StartCoroutine(TextActing("다양한 실험 도구를 활용해서 재밌는 실험을 할거에요!!", 4));
        lyricsText.text = "다양한 실험 도구를 활용해서 재밌는 실험을 할거에요!!";
        yield return new WaitForSeconds(4.5f);

        buttonPanel.SetActive(true);
        blowUpTellingPart = BlowUpTelling.alchol;

        audioSource.PlayOneShot(alcholAudio);
        //lyricsText.text = "알콜램프를 선택해주세요!! , 삼발이를 선택해주세요!!";
        //StartCoroutine(TextActing("알콜램프를 선택해주세요!! ", 2));
        lyricsText.text = "알콜램프를 선택해주세요!! ";
        //yield return new WaitForSeconds(3);
        //alcholLampButton.SetActive(true);


        while (!lampClicked)
        {
            yield return null;
        }
        // 알콜램프 등장

        correctButtonClicked = false;

        audioSource.PlayOneShot(alcholSelectedAudio);
        //StartCoroutine(TextActing("짜잔!! 알콜이 들어있는 알콜램프가 나타났어요!! ", 2));
        lyricsText.text = "짜잔!! 알콜이 들어있는 알콜램프가 나타났어요!! ";
        yield return new WaitForSeconds(3);
        //StartCoroutine(TextActing("알콜 심지 부분에 불을 붙히면 , 불이 알콜을 연료로 타오른답니다!!", 5));
        lyricsText.text = "알콜 심지 부분에 불을 붙히면 , 불이 알콜을 연료로 타오른답니다!!";
        yield return new WaitForSeconds(6);


        lampClicked = false;
        lyricsGameObject.SetActive(false);


        buttonPanel.SetActive(true);
        blowUpTellingPart = BlowUpTelling.threeLeg;
        lyricsGameObject.SetActive(true);
        //StartCoroutine(TextActing("삼발이를 선택해주세요!!", 2));
        lyricsText.text = "삼발이를 선택해주세요!!";
        yield return new WaitForSeconds(2);
        audioSource.PlayOneShot(threeLegAudio);
        //threeLegButton.SetActive(true);

        while (!threeLegClicked)
        {
            yield return null;
        }
        correctButtonClicked = false;

        threeLegClicked = false;
        audioSource.PlayOneShot(threeLegSelectedAudio);
        audioSource.PlayOneShot(successEffectSounds[1]);
        //StartCoroutine(TextActing("짜잔!! 삼발이가 나타났어요!! 플라스크나 비커를 올려둘 수 있는 도구에요!! ", 4.5f));
        lyricsText.text = "짜잔!! 삼발이가 나타났어요!! 플라스크나 비커를 올려둘 수 있는 도구에요!! ";
        yield return new WaitForSeconds(6);
        //StartCoroutine(TextActing("구멍이 뚫려있어서 , 알콜램프의 불을 받게 설치할 수 있습니다", 5));
        lyricsText.text = "구멍이 뚫려있어서 , 알콜램프의 불을 받게 설치할 수 있습니다";
        yield return new WaitForSeconds(6);

        //4sec

        buttonPanel.SetActive(true);
        blowUpTellingPart = BlowUpTelling.plask;
        audioSource.PlayOneShot(plaskAudio);
        lyricsGameObject.SetActive(true);
        lyricsText.text = "삼각 플라스크를 선택해주세요!!";
        //StartCoroutine(TextActing("삼각 플라스크를 선택해주세요!!", 2));

        yield return new WaitForSeconds(2.5f);
        //plaskButton.SetActive(true);
        while (!plaskClicked)
        {
            yield return null;
        }
        correctButtonClicked = false;

        lyricsGameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        plaskClicked = false;

        lyricsGameObject.SetActive(true);
        audioSource.PlayOneShot(plaskSelectedAudio);
        StartCoroutine(AirBlowUpAnimation());
        //StartCoroutine(TextActing("짜잔! 삼각 플라스크가 나타났어요!!", 2));
        lyricsText.text = "짜잔! 삼각 플라스크가 나타났어요!!";
        yield return new WaitForSeconds(3);
        //StartCoroutine(TextActing("액체를 넣을 수 있고 풍선을 씌울 수 있는 도구에요!!", 3));
        lyricsText.text = "액체를 넣을 수 있고 풍선을 씌울 수 있는 도구에요!!";
        yield return new WaitForSeconds(4);
        lyricsGameObject.SetActive(false);

        yield return new WaitForSeconds(1);


        lyricsGameObject.SetActive(true);
        audioSource.PlayOneShot(ballonToPlaskAudio);
        buttonPanel.SetActive(true);
        blowUpTellingPart = BlowUpTelling.ballon;
        lyricsText.text = "이제 열기구 역할을 하는 풍선을 플라스크에 씌워볼게요!!";
        //StartCoroutine(TextActing("이제 열기구 역할을 하는 풍선을 플라스크에 씌워볼게요!!", 3));
        yield return new WaitForSeconds(4);
        // ballonToPlaskButton.SetActive(true);
        while (!ballonToPlaskClicked)
        {
            yield return null;
        }
        correctButtonClicked = false;

        lyricsGameObject.SetActive(false);
        ballonToPlaskClicked = false;
        yield return new WaitForSeconds(1);
        lyricsGameObject.SetActive(true);
        audioSource.PlayOneShot(afterBallonToPlaskAudio);
        //StartCoroutine(TextActing("짜잔!! 플라스크 구멍에 맞춰 반투명한 풍선을 씌웠어요!! ", 3f));
        lyricsText.text = "짜잔!! 플라스크 구멍에 맞춰 반투명한 풍선을 씌웠어요!! ";
        yield return new WaitForSeconds(4);
        //StartCoroutine(TextActing("지금은 풍선이 쪼그라들어있어요!!", 3f));
        lyricsText.text = "지금은 풍선이 쪼그라들어있어요!!";
        yield return new WaitForSeconds(4);


        audioSource.PlayOneShot(showPlaskAir);
        //StartCoroutine(TextActing("플라스크 안에 공기들이 보이시나요?", 1.5f));
        lyricsText.text = "플라스크 안에 공기들이 보이시나요?";
        yield return new WaitForSeconds(2f);
        //StartCoroutine(TextActing("지금은 공기들이 천천히 움직이고 있어요!!", 4f));
        lyricsText.text = "지금은 공기들이 천천히 움직이고 있어요!!";
        yield return new WaitForSeconds(4.5f);


        lyricsGameObject.SetActive(false);
        yield return new WaitForSeconds(1);


        //5sec

        lyricsGameObject.SetActive(true);
        audioSource.PlayOneShot(IgniteToAlcholAudio);
        lyricsText.text = "알콜램프에 불을 붙혀 볼게요!! 풍선이 어떻게 될까요?? ";
        //StartCoroutine(TextActing("알콜램프에 불을 붙혀 볼게요!! 풍선이 어떻게 될까요?? ", 3));
        yield return new WaitForSeconds(4);
        buttonPanel.SetActive(true);
        blowUpTellingPart = BlowUpTelling.Ignite;

        //igniteButton.SetActive(true);
        while (!igniteClicked)
        {
            yield return null;
        }
        correctButtonClicked = false;

        lyricsGameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        audioSource.PlayOneShot(igniteSoundEffect);
        ballonBlowUpAnimator.SetBool("Fired", true);
        //ballonBlowUpAnimation
        //air Animation
        //igniteClicked = false;


        lyricsGameObject.SetActive(true);
        audioSource.PlayOneShot(ballonBlowUpAudio);
        lyricsText.text = "풍선이 부풀어 올랐어요! 잘했어요!! ";
        audioSource.PlayOneShot(successEffectSounds[1]);
        //StartCoroutine(TextActing("풍선이 부풀어 올랐어요! 잘했어요!!", 2.5f));
        yield return new WaitForSeconds(3f);

        lyricsText.text = "공기들은 이렇게 불을 만나면 돌아다니며 춤을 춘답니다 ! ";
        //StartCoroutine(TextActing("공기들은 이렇게 불을 만나면 돌아다니며 춤을 춘답니다 ! ", 4));
        yield return new WaitForSeconds(9);

        lyricsGameObject.SetActive(false);

        for (int x = 0; x < allGameObjects.Length; x++)
        {
            allGameObjects[x].SetActive(false);
        }
        chooseLabTypePanel.SetActive(true);

    }

    IEnumerator BlowDownTeller()
    {
        phase = Phase.phase2;

        if (playTypeManagerScript.playType == playTypeManager.PlayType.noMarker)
        {
            emptyBallon.SetActive(false);
            CameraSetting();
        }
        yield return new WaitForSeconds(1);

            audioSource.PlayOneShot(turnOffFireAudio);
            lyricsText.text = "이제 공기들을 진정시켜볼게요!! 먼저 불을 꺼주세요!! ";
            //StartCoroutine(TextActing("이제 공기들을 진정시켜볼게요!! 먼저 불을 꺼주세요!!", 3));
            yield return new WaitForSeconds(4);
            lyricsGameObject.SetActive(false);
            turnOffButton.SetActive(true);
            while (!turnOffFireClicked)
            {
                yield return null;
            }
            audioSource.PlayOneShot(congratulationAudio);
            lyricsGameObject.SetActive(true);
            lyricsText.text = "성공했어요!! 축하합니다!!";
            //StartCoroutine(TextActing("성공했어요!! 축하합니다!!", 2));
            yield return new WaitForSeconds(3);
            lyricsGameObject.SetActive(false);
            turnOffFireClicked = false;
            //4sec
            //turnOffButton
            lyricsGameObject.SetActive(true);
            audioSource.PlayOneShot(selectIceAudio);
            lyricsText.text = "얼음을 선택해주세요!! ";
            //StartCoroutine(TextActing("얼음을 선택해주세요!!", 2));
            yield return new WaitForSeconds(3);

            lyricsText.text = "얼음을 플라스크에 갖다 대면 , 온도가 낮아져 공기가 움직임을 멈출거에요!!";
            //StartCoroutine(TextActing("얼음을 플라스크에 갖다 대면 , 온도가 낮아져 공기가 움직임을 멈출거에요!!", 5));

            yield return new WaitForSeconds(6);
            iceButton.SetActive(true);
            while (!iceButtonClicked)
            {
                yield return null;
            }
            audioSource.PlayOneShot(iceSpawnEffectSound);
            lyricsGameObject.SetActive(false);
            plaskObj.SetActive(false);
            alcholLampObj.SetActive(false);
            threeLegObj.SetActive(false);
            explosionParticle.Play();
            iceBasket.gameObject.SetActive(true);
            iceBasket.gameObject.transform.position = spawnPosition.position;
            ballonBlowUpPlask.SetActive(true);
            ballonBlowUpPlask.transform.position = new Vector3(spawnPosition.position.x, spawnPosition.position.y + 0.0925f, spawnPosition.position.z);
            StartCoroutine(AirBlowDownAnimation());
            yield return new WaitForSeconds(0.5f);

            audioSource.PlayOneShot(plaskToIceAudio);
            lyricsGameObject.SetActive(true);
            //StartCoroutine(TextActing("짜잔!! , 얼음이 들어있는 비커를 가져왔어요!! ", 1.5f)) ;
            lyricsText.text = "짜잔!! , 얼음이 들어있는 비커를 가져왔어요!! ";
            yield return new WaitForSeconds(2);
            //StartCoroutine(TextActing("이제 풍선이 붙어있는 플라스크를 얼음에 올려 볼게요!!", 6));
            lyricsText.text = "이제 풍선이 붙어있는 플라스크를 얼음에 올려 볼게요!!";
            yield return new WaitForSeconds(7);
            plaskToIceButton.SetActive(true);

            while (!plaskToIceClicked)
            {
                yield return null;
            }
            plaskToIceClicked = false;
            iceButtonClicked = false;
            lyricsGameObject.SetActive(false);
            yield return new WaitForSeconds(1);
            ballonBlowUpPlask.transform.DOMove(iceBucketMeetPlaskPos.position, 2);
            yield return new WaitForSeconds(2);
            StartCoroutine(AirBlowDownAnimation());
            ballonBlowUpPlask.GetComponent<Animator>().SetBool("BlowDown", true);
            moveDone = true;
            audioSource.PlayOneShot(iceCrackingEffectSound);
            yield return new WaitForSeconds(1);
            audioSource.PlayOneShot(iceCrackingEffectSound);

            // air animation

            lyricsGameObject.SetActive(true);
            audioSource.PlayOneShot(plaskToIceDoneAudio);
            //StartCoroutine(TextActing("뜨겁게 활발히 움직이던 공기가 , 얼음을 만나서 움직임이 둔해젔어요!! ", 5));
            lyricsText.text = "뜨겁게 활발히 움직이던 공기가 , 얼음을 만나서 움직임이 둔해젔어요!! ";
            yield return new WaitForSeconds(6);
            //StartCoroutine(TextActing("정말 신기하네요!!", 1.5f));
            lyricsText.text = "정말 신기하네요!!";
            yield return new WaitForSeconds(2);
            moveDone = false;



            //3sec
            //8sec
            audioSource.PlayOneShot(successEffectSounds[1]);
            yield return new WaitForSeconds(0.5f);
            audioSource.PlayOneShot(endingSoundEffect);
            yield return new WaitForSeconds(1.5f);
            lyricsGameObject.SetActive(true);
            lyricsText.text = "성공했어요!! 축하해요!! 모든 실험을 마쳤어요!!";
            //StartCoroutine(TextActing("성공했어요!! 축하해요!! 모든 실험을 마쳤어요!!", 3));
            audioSource.PlayOneShot(endingOfLabAudio);
            yield return new WaitForSeconds(4);

            lyricsText.text = "다음에 다시만나요!! 안녕~!!!!";
            //StartCoroutine(TextActing("다음에 다시만나요!!! 안녕~!!!!", 2));
            yield return new WaitForSeconds(3);
            lyricsGameObject.SetActive(false);

            endRestartButton.SetActive(true);
            restartButton.SetActive(false);


        
    }
    
  

}
