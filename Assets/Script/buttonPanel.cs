﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonPanel : MonoBehaviour
{

    public float duration;

    void Start()
    {
        
    }

    private void OnEnable()
    {
        StartCoroutine(timeChecker());
    }
    IEnumerator timeChecker ()
    {
        while(this.gameObject.activeInHierarchy )
        {
            duration += Time.deltaTime;
            yield return null;
        }
    }

    private void OnDisable()
    {
        duration = 0;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
