﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBall : MonoBehaviour
{

    public GameObject ball;
    public Transform camTranform;
    public Transform shootPoint;

    // Start is called before the first frame update

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            print(0);
            ShootBallObj();
        }
    }



    public void ShootBallObj()
    {
        GameObject tObj = Instantiate(ball);
        tObj.transform.position = shootPoint.transform.position;
        Vector3 tVector = (shootPoint.transform.position - camTranform.transform.position).normalized;

        Rigidbody tRigidbody = tObj.GetComponent<Rigidbody>();
        tRigidbody.AddForce(tVector * 100f);
        
    }



}
