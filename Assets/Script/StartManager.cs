﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class StartManager : MonoBehaviour
{
    public GameObject coverImage;
    public GameObject selectWays;
    public GameObject document;
    public AudioClip buttonClickSound;
    public AudioSource audioSource;

   

    private void Start()
    {

    }


    public void OnClickDocument ()
    {
        audioSource.PlayOneShot(buttonClickSound);
        document.SetActive(true);
        

    }

    public void OnClickExitDocument()
    {
        audioSource.PlayOneShot(buttonClickSound);
        document.SetActive(false);
    }


    public void OnClickStartButton ()
    {
        audioSource.PlayOneShot(buttonClickSound);
        coverImage.SetActive(false);
        selectWays.SetActive(true);
    }

    public void OnClickStartWithMarker ()
    {
        audioSource.PlayOneShot(buttonClickSound);
        playTypeManager playTypeManagerScript = FindObjectOfType<playTypeManager>();
        playTypeManagerScript.playType = playTypeManager.PlayType.marker;
        SceneManager.LoadScene("Marker");
    }

    public void OnClickStartWithNothing()
    {
        playTypeManager playTypeManagerScript = FindObjectOfType<playTypeManager>();
        playTypeManagerScript.playType = playTypeManager.PlayType.noMarker;
        audioSource.PlayOneShot(buttonClickSound);
        SceneManager.LoadScene("Marker");
    }



}
